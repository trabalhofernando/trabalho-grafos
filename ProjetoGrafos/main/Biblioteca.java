
package br.com.trabalho.main;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import br.com.trabalho.algoritmos.BuscaEmLargura;
import br.com.trabalho.algoritmos.BuscaEmProfundidade;
import br.com.trabalho.algoritmos.CalcularDistancia;
import br.com.trabalho.algoritmos.Dijkstra;
import br.com.trabalho.arquivo.LerArquivo;
import br.com.trabalho.modelo.Aresta;
import br.com.trabalho.modelo.Grafo;
import br.com.trabalho.modelo.Vertice;

/**
 * Classe base pela qual é feita a leitura e é gerada a saida. Essa classe
 * possui um contrutor onde sao passados o caminho do arquivo de entrada, e o
 * caminho desejado para o arquivo de saida.
 * 
 */

public class Biblioteca {
	private String caminhoOrigem;
	private String caminhoDestino;

	public Biblioteca(String origem, String destino) {
		this.caminhoOrigem = origem;
		this.caminhoDestino = destino;
	}

	public void gerarSaida() throws IOException {
		// Instancias.

		LerArquivo lerArquivo = new LerArquivo(this.caminhoOrigem);
		CalcularDistancia calcularDistancia = new CalcularDistancia();

		List<Vertice> listaVertices = new ArrayList<>();
		List<Aresta> listaArestas = new ArrayList<>();

		lerArquivo.leitura();

		listaVertices = lerArquivo.getVertice();
		listaArestas = lerArquivo.getAresta();

		Grafo grafo = new Grafo(listaArestas, listaVertices);
		Dijkstra dijkstra = new Dijkstra(grafo);

		StringBuilder saida = new StringBuilder();

		saida.append("\t\t\t Arquivo de saída \n");
		saida.append("---------------------------------------------------------------------\n");
		saida.append("DISTANCIAS : \n");

		for (int i = 0; i < lerArquivo.getVetorDistancia().length; i++) {
			saida.append("\nDistancia " + lerArquivo.getVetorDistancia()[i].replace(",", " ") + " :" + "\n");
			if(calcularDistancia.calcularDistancia(listaVertices, lerArquivo.getVetorDistancia()[i]) == 0){
				saida.append("Distancia inexistente.");
			}else {
				saida.append(calcularDistancia.calcularDistancia(listaVertices, lerArquivo.getVetorDistancia()[i]));
			}
			saida.append("\n");
		}
		saida.append("---------------------------------------------------------------------\n");
		saida.append("BUSCA EM PROFUNDIDADE : \n");
		listaVertices = lerArquivo.getVertice();

		for (int i = 0; i <= lerArquivo.getVetorProfundidade().length - 1; i++) {
			BuscaEmProfundidade buscaEmProfundidade = new BuscaEmProfundidade();
			listaVertices = lerArquivo.getVertice();
			String aux = lerArquivo.getVetorProfundidade()[i];
			String[] vetorAux = aux.split(",");
			saida.append("\nProfundidade " + lerArquivo.getVetorProfundidade()[i].replace(",", " ") + ":" + "\n");

			if (buscaEmProfundidade.buscar(listaVertices, vetorAux[0], vetorAux[1]).getDescricao() != null) {
				saida.append(buscaEmProfundidade.getSaida());
			} else {
				saida.append("Vertice não encontrado.");
			}
			saida.append("\n");
		}

		saida.append("---------------------------------------------------------------------\n");
		saida.append("BUSCA EM LARGURA : \n");
		listaVertices = lerArquivo.getVertice();

		for (int i = 0; i <= lerArquivo.getVetorLargura().length - 1; i++) {
			BuscaEmLargura buscaEmLargura = new BuscaEmLargura();
			listaVertices = lerArquivo.getVertice();
			String aux = lerArquivo.getVetorLargura()[i];
			String[] vetorAux = aux.split(",");
			saida.append("Largura " + lerArquivo.getVetorLargura()[i].replace(",", " ") + ":" + "\n");
			if (buscaEmLargura.buscar(listaVertices, vetorAux[0], vetorAux[1]).getDescricao() != null) {
				saida.append(buscaEmLargura.getSaida());
			} else {
				saida.append("Vertice não encontrado.");
			}
			saida.append("\n");
		}

		saida.append("---------------------------------------------------------------------\n");
		listaVertices = lerArquivo.getVertice();
		saida.append("MENOR CAMINHO : \n");
		LinkedList<Vertice> caminho = new LinkedList<>();
		for (int i = 0; i <= lerArquivo.getVetorMenorCaminho().length - 1; i++) {
			int distancia = 0;
			saida.append("\nMenor caminho " + lerArquivo.getVetorMenorCaminho()[i].replace(",", " ") + ":");
			String aux = lerArquivo.getVetorMenorCaminho()[i];
			String[] vetorAux = aux.split(",");
			dijkstra.buscar(this.encontrarVertice(listaVertices, vetorAux[0]));
			caminho = dijkstra.getMenorCaminho(this.encontrarVertice(listaVertices, vetorAux[1]));

			String menorCaminho = "";

			if (caminho == null) {
				saida.append("\nCaminho inexistente.");
			}else {
				for (int j = 0; j <= caminho.size() - 1; j++) {
					menorCaminho += caminho.get(j).getDescricao() + ",";
				}
				distancia = calcularDistancia.calcularDistancia(listaVertices, menorCaminho);
				menorCaminho = menorCaminho.substring(0, menorCaminho.length() - 1);
				menorCaminho = menorCaminho.replace(",", " ");
				saida.append("\n" + menorCaminho);
				saida.append("\n" + distancia + "\n");	
			}
		}

		FileWriter fileWriter = new FileWriter(this.caminhoDestino + "/saida.txt");
		System.out.println("Arquivo salvo em : " + this.caminhoDestino + "saida.txt");
		fileWriter.write(saida.toString());
		fileWriter.close();
	}

	// Metodo usado para buscar um vertice dentro de uma lista de vertice.
	private Vertice encontrarVertice(List<Vertice> listaVertices, String id) {
		Vertice verticeEncontrado = new Vertice();

		for (int i = 0; i < listaVertices.size(); i++) {
			if (listaVertices.get(i).getDescricao().equals(id)) {
				verticeEncontrado = listaVertices.get(i);
			}
		}

		return verticeEncontrado;
	}

}
