package br.com.trabalho.main;

import java.io.IOException;

import br.com.trabalho.arquivo.LerArquivo;

public class Teste {
	public static void main(String[] args) {
		LerArquivo lerArquivo = new LerArquivo("/home/gds/Área de Trabalho/arquivoFernando.txt");
	
		try {
			lerArquivo.leitura();
			
			String entrada = args[0];
			System.out.println("Arquivo lido de : " + entrada);
			String saida = args[1];
			
			Biblioteca biblioteca = new Biblioteca(entrada, saida);
			biblioteca.gerarSaida();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
