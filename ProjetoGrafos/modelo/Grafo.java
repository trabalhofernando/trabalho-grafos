package br.com.trabalho.modelo;

import java.util.ArrayList;
import java.util.List;

public class Grafo {
	private List<Vertice> vertices = new ArrayList<Vertice>();
	private List<Aresta> arestas = new ArrayList<Aresta>();

	public Grafo(List<Aresta> arestas,List<Vertice> vertices) {
		this.vertices.addAll(vertices);
		this.arestas.addAll(arestas);
	}
	
	public Grafo(){
	}
	
	public void setArestas(List<Aresta> arestas) {
		this.arestas.addAll(arestas);
	}

	public List<Aresta> getArestas() {
		return arestas;
	}

	public void setVertices(List<Vertice> vertices) {
		this.vertices.addAll(vertices);
	}

	public List<Vertice> getVertices() {
		return this.vertices;
	}
}
