package br.com.trabalho.modelo;

import java.util.ArrayList;
import java.util.List;

public class Vertice implements Comparable<Vertice> {
	private String descricao;
	private int distancia;
	private boolean visitado = false;
	private Vertice pai;
	private List<Aresta> arestas = new ArrayList<Aresta>();
	private List<Vertice> vizinhos = new ArrayList<Vertice>();

	public Vertice() {

	}

	public Vertice(String nome) {
		this.descricao = nome;
	}

	public void setDescricao(String nome) {
		this.descricao = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void visitar() {
		this.visitado = true;
	}

	public boolean verificarVisita() {
		return visitado;
	}

	public void setDistancia(int distancia) {
		this.distancia = distancia;
	}

	public int getDistancia() {
		return this.distancia;
	}

	public void setPai(Vertice pai) {
		this.pai = pai;
	}

	public Vertice getPai() {
		return this.pai;
	}

	public void setVizinho(Vertice vizinho) {
		this.vizinhos.add(vizinho);
	}

	public List<Vertice> getVizinhos() {
		return this.vizinhos;
	}

	public void setAresta(Aresta novaAresta) {
		this.arestas.add(novaAresta);
	}

	public List<Aresta> getArestas() {
		return arestas;
	}

	@Override
	public int compareTo(Vertice vertice) {
		return this.getDescricao().compareTo(vertice.getDescricao());
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((descricao == null) ? 0 : descricao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vertice other = (Vertice) obj;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return descricao;
	}
}
