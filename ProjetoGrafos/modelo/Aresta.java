package br.com.trabalho.modelo;

/**
 * @author gds
 *
 */
public class Aresta {
	private int peso;
	private Vertice origem;
	private Vertice destino;

	public Aresta(Vertice v1, Vertice v2,int peso){
   		this.peso = peso;
		this.origem = v1;
		this.destino = v2;
	}
	
	public Vertice getOrigem() {
		return origem;
	}
	public void setOrigem(Vertice origem) {
		this.origem = origem;
	}
	public Vertice getDestino() {
		return destino;
	}
	public void setDestino(Vertice destino) {
		this.destino = destino;
	}
	public int getPeso() {
		return peso;
	}
	public void setPeso(int peso) {
		this.peso = peso;
	}
}
