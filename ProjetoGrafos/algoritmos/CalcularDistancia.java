package br.com.trabalho.algoritmos;

import java.util.List;

import br.com.trabalho.modelo.Vertice;

public class CalcularDistancia {
	private int distancia = 0;

	// Metodo usado para buscar um vertice dentro de uma lista de vertice.
	private Vertice encontrarVertice(List<Vertice> listaVertices, String id) {
		Vertice verticeEncontrado = new Vertice();

		for (int i = 0; i < listaVertices.size(); i++) {
			if (listaVertices.get(i).getDescricao().equals(id)) {
				verticeEncontrado = listaVertices.get(i);
			}
		}
		return verticeEncontrado;
	}

	public int calcularDistancia(List<Vertice> listaVertices, String distancias) {
		Vertice verticeOrigem = new Vertice();
		this.distancia = 0;
		
		String[] vetorDistancias = distancias.split(",");
		verticeOrigem = this.encontrarVertice(listaVertices, vetorDistancias[0]);

		int contador = 0;
		int contador2 = 0;
		for (int i = 0; i <= vetorDistancias.length - 2; i++) {
			verticeOrigem = this.encontrarVertice(listaVertices, vetorDistancias[i]);
			contador++;
			for (int j = 0; j <= verticeOrigem.getArestas().size() - 1; j++) {
				if (verticeOrigem.getArestas().get(j).getDestino().getDescricao().equals(vetorDistancias[contador])) {
					this.distancia += verticeOrigem.getArestas().get(j).getPeso();
					contador2++;
				}
			}
		}
		if (contador != contador2) {
			return this.distancia = 0;
		}else {
			return this.distancia;
		}
	}
}
