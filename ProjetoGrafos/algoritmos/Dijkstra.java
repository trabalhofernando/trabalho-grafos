package br.com.trabalho.algoritmos;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import br.com.trabalho.modelo.Aresta;
import br.com.trabalho.modelo.Grafo;
import br.com.trabalho.modelo.Vertice;

public class Dijkstra {
	private final List<Aresta> arestas;
	// Lista que irá armazenar os vertices que foram vizitados.
	private Set<Vertice> verticesVizitados;
	// Lista que irá armazenar os vertices que não foram vizitados.
	private Set<Vertice> verticesNaoVizitados;
	// Matriz responsavel por armazenar os vertices predecessores.
	private Map<Vertice, Vertice> predecessor;
	// Matriz responsavel por armazena as distancias entre os vertices.
	private Map<Vertice, Integer> distancia;

	// Construtor da classe Dijkstra.
	public Dijkstra(Grafo grafo) {
		this.arestas = new ArrayList<Aresta>(grafo.getArestas());
	}

	//Método responsável por fazer a busca do menor caminho.
	public void buscar(Vertice origem) {
		verticesVizitados = new HashSet<Vertice>();
		verticesNaoVizitados = new HashSet<Vertice>();
		distancia = new HashMap<Vertice, Integer>();
		predecessor = new HashMap<Vertice, Vertice>();
		distancia.put(origem, 0);
		verticesNaoVizitados.add(origem);
		while (verticesNaoVizitados.size() > 0) {
			Vertice verticeAtual = getMenor(verticesNaoVizitados);
			verticesVizitados.add(verticeAtual);
			verticesNaoVizitados.remove(verticeAtual);
			buscarMenorCaminho(verticeAtual);
		}
	}

	private void buscarMenorCaminho(Vertice vertice) {
		List<Vertice> verticesAdjacentes = getVizinhos(vertice);
		for (Vertice alvo : verticesAdjacentes) {
			if (getMenorDistancia(alvo) > getMenorDistancia(vertice) + getPeso(vertice, alvo)) {
				distancia.put(alvo, getMenorDistancia(vertice) + getPeso(vertice, alvo));
				predecessor.put(alvo, vertice);
				verticesNaoVizitados.add(alvo);
			}
		}
	}

	private int getPeso(Vertice vertice, Vertice target) {
		for (Aresta aresta : arestas) {
			if (aresta.getOrigem().equals(vertice) && aresta.getDestino().equals(target)) {
				return aresta.getPeso();
			}
		}
		throw new RuntimeException("Ocorreu um erro em tempo de execução.");
	}

	 //Vou alterar o funcionamento desse metodo para ser usado a lista de vizinhos de cada vertice.
	  private List<Vertice> getVizinhos(Vertice vertice) {
	    List<Vertice> vizinhos = new ArrayList<Vertice>();
	    for (Aresta aresta : arestas) {
	      if (aresta.getOrigem().equals(vertice)
	          && !getFoiVizitado(aresta.getDestino())) {
	        vizinhos.add(aresta.getDestino());
	      }
	    }
	    return vizinhos;
	  }


	private Vertice getMenor(Set<Vertice> vertices) {
		Vertice menor = null;
		for (Vertice vertice : vertices) {
			if (menor == null) {
				menor = vertice;
			} else {
				if (getMenorDistancia(vertice) < getMenorDistancia(menor)) {
					menor = vertice;
				}
			}
		}
		return menor;
	}

	// Vou alterar a logica desse metodo tambem.
	private boolean getFoiVizitado(Vertice vertex) {
		return verticesVizitados.contains(vertex);
	}

	private int getMenorDistancia(Vertice destination) {
		Integer d = distancia.get(destination);
		if (d == null) {
			return Integer.MAX_VALUE;
		} else {
			return d;
		}
	}

	public LinkedList<Vertice> getMenorCaminho(Vertice destino) {
		LinkedList<Vertice> caminho = new LinkedList<Vertice>();
		Vertice vertice = destino;
		// Checa se o caminho existe.
		if (predecessor.get(vertice) == null) {
			return null;
		}
		caminho.add(vertice);
		while (predecessor.get(vertice) != null) {
			vertice = predecessor.get(vertice);
			caminho.add(vertice);
		}
		//Ordena de forma correta para a exibição.
		Collections.reverse(caminho);
		return caminho;
	}
}
