package br.com.trabalho.algoritmos;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import br.com.trabalho.modelo.Vertice;

public class BuscaEmLargura {
	private Queue<Vertice> filaVizinhos = new LinkedList<>();
	private Vertice verticeEncontrado = new Vertice();
	private String origem = "";
	private String saida = "";

	// Metodo usado para buscar um vertice dentro de uma lista de vertice.
	private Vertice encontrarVertice(List<Vertice> listaVertices, String id) {
		Vertice verticeEncontrado = new Vertice();

		for (int i = 0; i < listaVertices.size(); i++) {
			if (listaVertices.get(i).getDescricao().equals(id)) {
				verticeEncontrado = listaVertices.get(i);
			}
		}
		return verticeEncontrado;
	}

	public Vertice buscar(List<Vertice> listaVertices, String origem, String verticeDesejado) {
		this.saida = "";
		this.origem = origem;
		this.saida += origem + "\n";
		Vertice verticeAtual = new Vertice();

		verticeAtual = this.encontrarVertice(listaVertices, origem);

		filaVizinhos.add(verticeAtual);

		Vertice verticeAux = new Vertice();
 		Vertice verticeAntecessor = new Vertice();

		verticeAntecessor = verticeAtual;

		while (!filaVizinhos.isEmpty()) {
			verticeAtual = filaVizinhos.peek();
			verticeAtual.setPai(verticeAntecessor);
			verticeAtual.visitar();

			Collections.sort(verticeAtual.getVizinhos());
			@SuppressWarnings("unchecked")
			List<Vertice> listaAux = (List<Vertice>) filaVizinhos;
			//Logica para gerar a visualização.
			for (int i = 0; i <= listaAux.size() - 1; i++) {
				if (!verticeAtual.getDescricao().equals(listaAux.get(i).getDescricao())) {
					this.saida += listaAux.get(i).getDescricao() + " ";
				}
			}
			
			for (int i = 0; i <= verticeAtual.getVizinhos().size() - 1; i++) {
				verticeAux = this.encontrarVertice(listaVertices, verticeAtual.getVizinhos().get(i).getDescricao());
				
				if (!verticeAux.verificarVisita()) {
					verticeAux.setPai(verticeAtual);
					filaVizinhos.add(verticeAux);
				}
				this.saida += verticeAux.getDescricao() + " ";
			}
			this.saida += "\n";
			if (verticeAux.getDescricao().equals(verticeDesejado)) {
				verticeAux.setPai(verticeAtual);
				this.verticeEncontrado = verticeAux;
				this.saida += verticeEncontrado.getDescricao() + "\n";
				break;
			}
			verticeAntecessor = verticeAtual;
			filaVizinhos.poll();
		}
		return this.verticeEncontrado;
	}
	
	public String gerarTrajeto(){
		String trajeto = "";
		Vertice verticeEncontrado = new Vertice();
		verticeEncontrado = this.verticeEncontrado;
		Vertice verticeAux = new Vertice();
		
		String[] auxiliar;
		while (!verticeEncontrado.getDescricao().equals(this.origem)) {
			verticeAux = verticeEncontrado.getPai();
			trajeto += verticeEncontrado.getDescricao() + ",";
			verticeEncontrado = verticeAux;
		}
		
		trajeto += verticeEncontrado.getDescricao() + ",";
		trajeto = trajeto.substring(0,trajeto.length()-1);
		
		auxiliar = trajeto.split(",");
		trajeto = "";

		for (int i = auxiliar.length - 1; i >= 0; i--) {
			trajeto += auxiliar[i]+ ",";
			
		}
		trajeto = trajeto.substring(0,trajeto.length()-1);
		trajeto = trajeto.replace(",", "-->");
		
		return trajeto;
	}

	public String getSaida() {
		return saida;
	}

	public void setSaida(String saida) {
		this.saida = saida;
	}
	
}
