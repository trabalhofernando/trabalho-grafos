package br.com.trabalho.arquivo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import br.com.trabalho.modelo.Aresta;
import br.com.trabalho.modelo.Vertice;

public class LerArquivo {
	private File arquivo;

	private String vertices = "";
	private Boolean grafoDirecionado;
	private Boolean grafoPeso;
	private String arestas = "";
	private String[] vetorDistancia;
	private String[] vetorProfundidade;
	private String[] vetorLargura;
	private String[] vetorMenorCaminho;
	private String prim = "";
	private String kruskal = "";

	// Contrutor que recebe o arquivo a ser lido.
	public LerArquivo(String arquivo) {
		this.arquivo = new File(arquivo);
	}

	// Lê todo o conteudo do arquivo.
	public String lerTudo() throws IOException {
		BufferedReader bufferedReader = new BufferedReader(new FileReader(this.arquivo));
		String linha = "";
		String saida = "";
		while (true) {
			if (linha != null) {
				linha = bufferedReader.readLine();
				saida += "\n" + linha;
			} else {
				break;
			}
		}
		bufferedReader.close();
		return saida;
	}

	// Metodo que le linha a linha e atribui no campo correspondente.
	public void leitura() throws IOException {
		BufferedReader bufferedReader = new BufferedReader(new FileReader(this.arquivo));
		String linha = "";

		// Le a primeira linha no caso GRAFO.
		linha = bufferedReader.readLine();
		// Le a linha que contem os dados dos vertices.
		this.vertices = bufferedReader.readLine();
		// Le os dados relacionados ao direcionamento do grafo.
		linha = bufferedReader.readLine();
		if (linha.equals("true ;")) {
			this.grafoDirecionado = true;
		} else if (linha.equals("false ;")) {
			this.grafoDirecionado = false;
		}
		// Le os dados sobre os pesos dos grafos.
		linha = bufferedReader.readLine();
		if (linha.equals("true ;")) {
			this.grafoPeso = true;
		} else if (linha.equals("false ;")) {
			this.grafoPeso = false;
		}
		// Le a linha em branco que separa as conf do grafo com as info das
		// arestas
		linha = bufferedReader.readLine();
		// Le a linha ARESTAS
		linha = bufferedReader.readLine();

		// Lê todos os dados relacionados as ARESTAS.
		while (true) {
			linha = bufferedReader.readLine();
			if (linha.charAt(linha.length() - 1) == ';') {
				this.arestas += linha + '\n';
				break;
			} else {
				this.arestas += linha + '\n';
			}
		}

		// Le a linha em branco.
		linha = bufferedReader.readLine();
		// Le a linha COMANDOS
		linha = bufferedReader.readLine();
		// Le os comandos e guarda na string correspondente.
		String distancia = "";
		String profundidade = "";
		String largura = "";
		String menorCaminho = "";
		while (true) {
			linha = bufferedReader.readLine();
			if (linha != null && linha.length() > 3) {
				if (linha.substring(0, 3).equals("DIS")) {
					distancia += linha + "\n";
				} else if (linha.substring(0, 3).equals("PRO")) {
					profundidade += linha + "\n";
				} else if (linha.substring(0, 3).equals("LAR")) {
					largura += linha + "\n";
				} else if (linha.substring(0, 3).equals("MEN")) {
					menorCaminho += linha + "\n";
				} else if (linha.substring(0, 3).equals("PRI")) {
					this.prim += linha + "\n";
				} else {
					this.kruskal += linha + "\n";
				}
			} else {
				break;
			}
		}
		// Cria um vetor com os dados de distancia.
		if (distancia.length() > 0) {
			this.vetorDistancia = distancia.split("\n");
			for (int i = 0; i < vetorDistancia.length; i++) {
				vetorDistancia[i] = vetorDistancia[i].replace("DISTANCIA", "");
				vetorDistancia[i] = vetorDistancia[i].replace(";", "");
				vetorDistancia[i] = vetorDistancia[i].substring(1, vetorDistancia[i].length());
				vetorDistancia[i] = vetorDistancia[i].replace(" ", ",");
			}
		}

		if (largura.length() > 0) {
			this.vetorLargura = largura.split("\n");
			for (int i = 0; i < vetorLargura.length; i++) {
				vetorLargura[i] = vetorLargura[i].replace("LARGURA", "");
				vetorLargura[i] = vetorLargura[i].replace(";", "");
				vetorLargura[i] = vetorLargura[i].substring(1, vetorLargura[i].length());
				vetorLargura[i] = vetorLargura[i].replace(" ", ",");
			}
		}

		if (profundidade.length() > 0) {
			this.vetorProfundidade = profundidade.split("\n");
			for (int i = 0; i < vetorProfundidade.length; i++) {
				vetorProfundidade[i] = vetorProfundidade[i].replace("PROFUNDIDADE", "");
				vetorProfundidade[i] = vetorProfundidade[i].replace(";", "");
				vetorProfundidade[i] = vetorProfundidade[i].substring(1, vetorProfundidade[i].length());
				vetorProfundidade[i] = vetorProfundidade[i].replace(" ", ",");
			}
		}
		
		if (menorCaminho.length() > 0) {
			this.vetorMenorCaminho = menorCaminho.split("\n");
			for (int i = 0; i < vetorMenorCaminho.length; i++) {
				vetorMenorCaminho[i] = vetorMenorCaminho[i].replace("MENOR CAMINHO", "");
				vetorMenorCaminho[i] = vetorMenorCaminho[i].replace(";", "");
				vetorMenorCaminho[i] = vetorMenorCaminho[i].substring(1, vetorMenorCaminho[i].length());
				vetorMenorCaminho[i] = vetorMenorCaminho[i].replace(" ", ",");
			}
		}
		
		bufferedReader.close();
	}

	public List<Vertice> getVertice() {
		return listaVerticeCompleta(this.gerarListaVertice(), this.gerarListaArestas(this.gerarListaVertice()));
	}

	public List<Aresta> getAresta() {
		return this.gerarListaArestas(this.gerarListaVertice());
	}

	// Gera a lista de vertices e cria os objetos necessarios.
	private List<Vertice> gerarListaVertice() {
		// Recebe nessa string os dados referente aos vertices que estavam no
		// arquivo.
		String verticesLidos = this.getVertices();
		// Remove o ; da string.
		verticesLidos = verticesLidos.replace(";", "");
		// Cria um vetor usando o " " como regex.
		String[] nomeVertices = verticesLidos.split(" ");
		// Lista que sera a saida do metodo.
		List<Vertice> vertices = new ArrayList<>();

		// Cria os objetos necessarios e os adiciona na lista de vertices.
		for (int i = 0; i < nomeVertices.length; i++) {
			vertices.add(new Vertice(nomeVertices[i].substring(0, nomeVertices[i].length())));
		}
		// Retorna a lista com os vertices.
		return vertices;
	}

	// Gera uma lista de vertice completa com os dados de vertices vizinhos e
	// arestas pertencentes ao vertice.
	private List<Vertice> listaVerticeCompleta(List<Vertice> atual, List<Aresta> arestas) {
		// Retorno do método.
		List<Vertice> listaCompleta = new ArrayList<>();

		// Vertice de apoio.
		Vertice vertice = new Vertice();

		// Logica para preencher as arestas pertences a aquele vertice.
		for (int i = 0; i < atual.size(); i++) {
			vertice = atual.get(i);
			for (int j = 0; j < arestas.size(); j++) {
				if (vertice.getDescricao().equals(arestas.get(j).getOrigem().getDescricao())) {
					vertice.setAresta(arestas.get(j));
				}
				if (vertice.getDescricao().equals(arestas.get(j).getDestino().getDescricao())) {
					vertice.setAresta(arestas.get(j));
				}
			}
			listaCompleta.add(vertice);
		}

		// Verifica se o grafo é direcionado.
		if (!this.grafoDirecionado) {
			// Logica para adicionar na lista de vertices vizinhos.
			for (int i = 0; i < listaCompleta.size(); i++) {
				vertice = listaCompleta.get(i);
				for (int j = 0; j < vertice.getArestas().size(); j++) {
					if (vertice.getArestas().get(j).getOrigem().getDescricao().equals(vertice.getDescricao())) {
						vertice.setVizinho(listaCompleta.get(i).getArestas().get(j).getDestino());
					}
					if (vertice.getArestas().get(j).getDestino().getDescricao().equals(vertice.getDescricao())) {
						vertice.setVizinho(listaCompleta.get(i).getArestas().get(j).getOrigem());
					}
				}
				listaCompleta.get(i).setVizinho(vertice);
			}
		} else {
			// Logica para adicionar na lista de vertices vizinhos.
			for (int i = 0; i < listaCompleta.size(); i++) {
				vertice = listaCompleta.get(i);
				for (int j = 0; j < vertice.getArestas().size(); j++) {
					if (vertice.getArestas().get(j).getOrigem().getDescricao().equals(vertice.getDescricao())) {
						vertice.setVizinho(listaCompleta.get(i).getArestas().get(j).getDestino());
					}
				}
				listaCompleta.get(i).setVizinho(vertice);
			}
		}

		Vertice aux = new Vertice();

		// Remover o proprio vertice da lista.
		for (int i = 0; i < listaCompleta.size(); i++) {
			aux = listaCompleta.get(i);
			for (int j = 0; j < aux.getVizinhos().size(); j++) {
				if (aux.getVizinhos().contains(listaCompleta.get(i).getVizinhos().get(j))) {
					listaCompleta.get(i).getVizinhos().remove(aux);
				}
			}
		}
		// Lista completa dos vertices.s
		return listaCompleta;
	}

	// Preenche todo a lista com os dados lidos do arquivo.
	private List<Aresta> gerarListaArestas(List<Vertice> vertices) {
		// String que contem as arestas lidas do arquivo.
		String[] vetor = this.gerarArray(this.getArestas(), "\n");
		// Lista de arestas que sera o retorno do metodo.
		List<Aresta> arestas = new ArrayList<>();
		// Vertices auxiliares.
		Vertice verticeAux = new Vertice();
		Vertice verticeAux2 = new Vertice();
		// Busca a lista de vertices lidas do arquivo.
		vertices = this.gerarListaVertice();
		// Vetor que armazenara os dados de cada linha.
		String[] vetorString = null;
		// Logica responsável por criar o vetor e preencher a lista de aresta.
		for (int i = 0; i < vetor.length; i++) {
			for (int j = 0; j < vertices.size(); j++) {
				String stringAux = vetor[i];
				vetorString = stringAux.split(",");

				if (vertices.get(j).getDescricao().equals(vetorString[0])) {
					verticeAux = vertices.get(j);
				}
				if (vertices.get(j).getDescricao().equals(vetorString[1])) {
					verticeAux2 = vertices.get(j);
				}
			}
			// Se tiver peso esse grafo eu adiciono o que eu li do arquivo
			if (this.grafoPeso) {
				arestas.add(new Aresta(verticeAux, verticeAux2, Integer.parseInt(vetorString[2])));
				// Senão adiciono 1 no lugar do peso.
			} else {
				arestas.add(new Aresta(verticeAux, verticeAux2, 1));
			}
		}
		return arestas;
	}

	// Função para criar um array passando um string como parametro.
	private String[] gerarArray(String string, String split) {
		// Vetor que sera o retorno do metodo.
		String[] vetor = null;
		// Cria um array da string com o regex informado no parametro.
		vetor = string.split(split);
		// Faz a limpeza dos espaços e ponto e virgula para facilitar a logica
		// da criação das listas.
		for (int i = 0; i < vetor.length; i++) {
			vetor[i] = vetor[i].replace(" ", ",");
			vetor[i] = vetor[i].replace(";", ",");
		}
		// Vetor que sera retornado.
		return vetor;
	}

	public File getArquivo() {
		return arquivo;
	}

	public void setArquivo(File arquivo) {
		this.arquivo = arquivo;
	}

	private String getVertices() {
		return vertices;
	}

	public Boolean getGrafoDirecionado() {
		return grafoDirecionado;
	}

	public void setGrafoDirecionado(Boolean grafoDirecionado) {
		this.grafoDirecionado = grafoDirecionado;
	}

	public Boolean getGrafoPeso() {
		return grafoPeso;
	}

	public void setGrafoPeso(Boolean grafoPeso) {
		this.grafoPeso = grafoPeso;
	}

	private String getArestas() {
		return arestas;
	}

	public String[] getVetorDistancia() {
		return vetorDistancia;
	}

	public void setVetorDistancia(String[] vetorDistancia) {
		this.vetorDistancia = vetorDistancia;
	}


	public String[] getVetorProfundidade() {
		return vetorProfundidade;
	}

	public void setVetorProfundidade(String[] vetorProfundidade) {
		this.vetorProfundidade = vetorProfundidade;
	}

	public String[] getVetorLargura() {
		return vetorLargura;
	}

	public String[] getVetorMenorCaminho() {
		return vetorMenorCaminho;
	}

	public void setVetorMenorCaminho(String[] vetorMenorCaminho) {
		this.vetorMenorCaminho = vetorMenorCaminho;
	}

	public void setVetorLargura(String[] vetorLargura) {
		this.vetorLargura = vetorLargura;
	}

	public String getPrim() {
		return prim;
	}

	public void setPrim(String prim) {
		this.prim = prim;
	}

	public String getKruskal() {
		return kruskal;
	}

	public void setKruskal(String kruskal) {
		this.kruskal = kruskal;
	}
}