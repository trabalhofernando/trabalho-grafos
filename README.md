#**TP02_bibliotecagrafos**

![enter image description here](https://lh3.googleusercontent.com/-zQXMVB74k0Q/Vji14M4MTCI/AAAAAAAAIEc/yGUw1uGH-Cg/s0/grafo.jpg "grafo.jpg")


**Ciência da Computação, 6° Periódo - Pitagoras Betim**


1. Lucas Francisco
2. Nelson Júnior
3. Dionne Marciel
4. Socratés Magalhães
5. Gabriel Guedes 

 Professor: Fernando Augusto Fernandes Braz

 **26/11/2015**



## **Sobre o trabalho**
O grupo, em senso comum, decidiu que utilizariamos JAVA como plataforma para nosso trabalho. 
Uma vez que todos possuem ao menos noção base para o desenvolvimento do trabalho.
Utilizamos o Bitbucket para versionamento e gerenciamento do nosso trabalho, uma vez que o mesmo possui visibilidade privada, comportando até 5 usuários.


## **Principais dificuldades**
Encontramos duas dificuldades principais, a exibição gráfica do grafo e o nível de orientação a objeto.
 1. Exibição gráfica: após pesquisar por diversas bibliotecas que conseguissem criar a exibição gráfica do grafo, buscando entender o funcionamento das mesmas, encontramos a biblioteca D3 que gerava a visualização a partir de um arquivo json, à partir dai ficou um pouco mais fácil.
 2. Gerar a visualização no padrão desejado, pois tinhamos criado uma visualização baseado no caminho percorrido pelo grafo e acabou sendo necessário modifica-la de ultima hora.

###**Como funciona**
O repositório contem todas as classes necessárias para a criação da biblioteca, basta gerar o .jar lembrado de selecionar a classe Teste.java como a main e depois na linha de comando digitar o seguinte comando : java -jar NomeDaBiblioteca.jar /CaminhoDoArquivo/ /CaminhoParaSalvarSaida/ e pronto é gerado um txt com as saídas.

## **Desenvolvimento do trabalho**

Todos contribuiram um pouco com o desenvolvimento do trabalho, buscamos explorar as facilidades que cada um possui, e cada um ajudou de uma forma.
Os pontos extras foram distribuidos entre os integrantes de grupo, para que os mesmos pesquisassem e tentassem entender seu funcionamento e forma de utilização.

## **Atividades extras realizadas**
1. Git
2. Bitbucket
3. Markdown
4. Interface gráfica